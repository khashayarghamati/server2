package com.ghamati.calculate_server.controllers;

import com.ghamati.calculate_server.matlab.CalculateFormula;
import com.ghamati.calculate_server.models.VectorModel;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class RouteControllers {


    @RequestMapping(method = RequestMethod.POST, value = "/multiply/")
    public String multiply_vectors(@RequestBody VectorModel rootModel ){
        return CalculateFormula.multiply_vectors(rootModel);
    }
}

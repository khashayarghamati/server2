package com.ghamati.calculate_server;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CalculateServerApplication {

    public static void main(String[] args) {
        SpringApplication.run(CalculateServerApplication.class, args);
    }
}

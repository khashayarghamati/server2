package com.ghamati.calculate_server.matlab;

import com.ghamati.calculate_server.models.VectorModel;

import java.util.Arrays;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;

public class CalculateFormula {


    public static String multiply_vectors(VectorModel vectorModel){
        if (vectorModel.getFirst_vector().length != vectorModel.getSecond_vector().length)
            return "Size of vectors must be equal together";

        return call_matlab_api(vectorModel);

    }

    private static String call_matlab_api(VectorModel models){
        try {
            Matlab.getMatlab().putVariableAsync("A", models.getFirst_vector());
            Matlab.getMatlab().putVariableAsync("B", models.getSecond_vector());
            Matlab.getMatlab().evalAsync("C=A.*B");

            Future<double[]> C = Matlab.getMatlab().getVariableAsync("C");
            return Arrays.toString(C.get());

        } catch (InterruptedException | ExecutionException e) {
            e.printStackTrace();
            return "Matlab Error";
        }
    }
}

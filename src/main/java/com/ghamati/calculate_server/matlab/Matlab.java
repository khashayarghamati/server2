package com.ghamati.calculate_server.matlab;

import com.mathworks.engine.*;

import java.util.concurrent.ExecutionException;

public class Matlab {

    private Matlab(){}

    static class EngineHolder{
        static MatlabEngine matlabEngine;

        static {
            try {
                matlabEngine = MatlabEngine.startMatlabAsync().get();
            } catch (InterruptedException e) {
                e.printStackTrace();
            } catch (ExecutionException e) {
                e.printStackTrace();
            }
        }

    }

    public static MatlabEngine getMatlab(){
        return EngineHolder.matlabEngine;
    }

}

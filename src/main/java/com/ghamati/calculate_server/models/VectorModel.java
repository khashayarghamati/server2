package com.ghamati.calculate_server.models;

public class VectorModel {

    private double[] first_vector;

    public VectorModel(double[] first_vector) {
        this.first_vector = first_vector;
    }

    public VectorModel() {

    }
    public double[] getFirst_vector() {
        return first_vector;
    }

    public void setFirst_vector(double[] first_vector) {
        this.first_vector = first_vector;
    }

    public double[] getSecond_vector() {
        return second_vector;
    }

    public void setSecond_vector(double[] second_vector) {
        this.second_vector = second_vector;
    }

    private double[] second_vector;

    public VectorModel(double[] first_vector, double[] second_vector) {
        this.first_vector = first_vector;
        this.second_vector = second_vector;
    }
}
